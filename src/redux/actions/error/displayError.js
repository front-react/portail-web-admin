import { DISPLAY_ERROR } from "../../types";

export const displayError = (message, status) => {
  return dispatch => {
    return dispatch({
      type: DISPLAY_ERROR,
      error: {
        message,
        status,
      },
    });
  };
};

export default displayError;
