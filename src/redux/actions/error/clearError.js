import { CLEAR_ERROR } from "../../types";

export const clearError = () => {
  return dispatch => {
    return dispatch({
      type: CLEAR_ERROR,
    });
  };
};

export default clearError;
