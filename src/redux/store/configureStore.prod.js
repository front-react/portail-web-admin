import { createStore, applyMiddleware } from "redux";
//import reducers from '../reducers'
import reduxThunk from "redux-thunk";

/**
 * Configure dev store
 */
export default function configureStore(initialState) {
  let middleware = [reduxThunk];
  const createStoreWithMiddleware = applyMiddleware(...middleware)(createStore);

  const store = createStoreWithMiddleware(
    //reducers,
    initialState,
  );

  return store;
}
