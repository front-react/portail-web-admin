const initial = {};

function app(state = initial, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default app;
