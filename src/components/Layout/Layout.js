import React, { Component } from "react";
import { Route } from "react-router-dom";
import Header from "./Header";
import Home from "./../Home";
import Menu from "./Menu";

class Layout extends Component {
  render() {
    return (
      <div id="outer-container">
        <Header />
        <Menu />
        <main id="page-wrap">
          {this.props.children}
          <Route exact path="/" component={Home} />
        </main>
      </div>
    );
  }
}

export default Layout;
