import React, { PureComponent } from "react";
import { push as MenuContent } from "react-burger-menu";
import "./Menu.css";

class Menu extends PureComponent {
  render() {
    return (
      <MenuContent
        pageWrapId={"page-wrap"}
        outerContainerId={"outer-container"}
      >
        <a id="home" className="menu-item" href="/">
          Home
        </a>
        <a id="about" className="menu-item" href="/about">
          About
        </a>
        <a id="contact" className="menu-item" href="/contact">
          Contact
        </a>
      </MenuContent>
    );
  }
}

export default Menu;
