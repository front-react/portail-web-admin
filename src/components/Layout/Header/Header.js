import React, { PureComponent } from "react";
import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";
import "./Header.css";

class Header extends PureComponent {
  render() {
    return (
      <AppBar position="static">
        <Toolbar>
          <div className="toolbar-title">
            <Typography type="title">Up Admin</Typography>
          </div>
        </Toolbar>
      </AppBar>
    );
  }
}

export default Header;
