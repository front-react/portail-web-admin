import React, { Component } from "react";
import { Route } from "react-router-dom";
import { MuiThemeProvider, createMuiTheme } from "material-ui/styles";
import Layout from "./../Layout";
import { blue, orange, grey } from "material-ui/colors";

const theme = createMuiTheme({
  fontFamily: "Roboto Light, sans-serif",
  fontWeight: "200",
  palette: {
    primary: blue,
    secondary: orange,
    textColor: grey[900],
  },
});

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        {this.props.children}

        <Route path="/" component={Layout} />
      </MuiThemeProvider>
    );
  }
}

export default App;
